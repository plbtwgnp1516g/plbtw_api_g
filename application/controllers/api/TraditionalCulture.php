<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class TraditionalCulture extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$this->load->model('User_model');
		$this->load->model('Votes_model');
		$this->load->model('Konten_model');
    }
	
	public function userData_get(){
		$id_user=$this->get('id_user');
		
		$userData = $this->User_model->GetUserData($id_user);
		
		if($userData != NULL){
			$message = [
				'userData' => $userData,
				'status' => 1,
				'message' => 'User Data Succesfully Retrieved',
			];
		}else{
			$message = [
				'userData' => 'User not Found',
				'status' => 0,
				'message' => 'Get User Data Failed',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function totalvotes_get()
    {
        $id = $this->get('id');

        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response('Incorrect id_user', REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
		else if($id > 0 && $id != NULL){
			$totalrates = $this->User_model->GetTotalVotes($id);
			
			if($totalrates == NULL){
				$message = [
					'totalRates' => 'No Data Found',
					'status' => 0,
					'message' => 'Get Total Rates Failed',
				];
			}else{
				$message = [
					'totalRates' => $totalrates,
					'status' => 1,
					'message' => 'Get Total Rates Succeed',
				];
			}
			$this->set_response($message, REST_Controller::HTTP_CREATED);
		}
    }
	
    public function totalpoints_get()
    {
        $id = $this->get('id');

        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response('Incorrect id_user', REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
		else if($id > 0 && $id != NULL){
			$totalpoints = $this->User_model->GetTotalPoints($id);
			
			if($totalpoints == NULL){
				$message = [
					'totalPoints' => 'No Data Found',
					'status' => 0,
					'message' => 'Get Total Rates Failed',
				];
			}else{
				$message = [
					'totalPoints' => $totalpoints,
					'status' => 1,
					'message' => 'Get Total Rates Succeed',
				];
			}
			$this->set_response($message, REST_Controller::HTTP_CREATED);
		}
    }
	
	public function signup_post(){
		
		$email=$this->post('email');
		$fullname = $this->post('fullname');
		$password=$this->post('password');
		
		$resultquery = $this->User_model->SignUp($email,$password,$fullname);
		
		if($resultquery == false){
			$message = [
				'status'=>0,
				'message'=>'Failed to Add User',
			];
		}else if($resultquery == true){
			$message = [
				'status'=>1,
				'message'=>'user successfully added',
			];
		}
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function login_post(){
		$email=$this->post('email');
		$password=$this->post('password');
		
		$userData = $this->User_model->Login($email,$password);
		
		if($userData != NULL){
			$message = [
				'userData' => $userData,
				'status' => 1,
				'message' => 'Login Succeed',
			];
		}else{
			$message = [
				'userData' => 'User not Found',
				'status' => 0,
				'message' => 'Login Not Succeed',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function redeemChallenge_post(){
		$id_user = $this->post('id_user');
		$challengeno = $this->post('challengeno');
		$addpoint = $this->post('addpoint');
		
		$userData = $this->User_model->RedeemChallenge($id_user,$challengeno,$addpoint);
		
		if($userData != NULL){
			$message =[
				'userData' => $userData,
				'status' => 1,
				'message' => 'Points Added',
			];
		}else{
			$message =[
				'userData' => 'Point not Added',
				'status' => 0,
				'message' => 'User Not Found',
			];
		}
		
		$this->set_response($message,REST_Controller::HTTP_CREATED);
	}
	
	public function redeemRewards_post(){
		$id_user = $this->post('id_user');
		$minuspoint = $this->post('minuspoint');
                $redeemdate = $this->post('redeemdate');
                $productname = $this->post('productname');
		
		$userData = $this->User_model->RedeemRewards($id_user,$minuspoint,$redeemdate,$productname);
		
		if($userData != NULL && $minuspoint>0){
			$message =[
				'userData' => $userData,
				'status' => 1,
				'message' => 'Succesfully Redeem Reward',
			];
		}else if($userData == NULL || $minuspoint < 0){
			$message =[
				'userData' => 'failed to redeem',
				'status' => 0,
				'message' => 'Redeem Reward not Success',
			];
		}
		
		$this->set_response($message,REST_Controller::HTTP_CREATED);
	}
	
	public function rateUp_post(){
		$id_user = $this->post('id_user');
		$id_konten = $this->post('id_konten');
		
		$message = [
			'status'=>1,
			'message'=>'Rate Up Success',
		];
		
		$this->Votes_model->RatesUp($id_user,$id_konten);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function rateDown_post(){
		$id_user = $this->post('id_user');
		$id_konten = $this->post('id_konten');
		
		$message = [
			'status'=>1,
			'message'=>'Rate Down Success',
		];
		
		$this->Votes_model->RatesDown($id_user,$id_konten);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function rateNormal_post(){
		$id_user = $this->post('id_user');
		$id_konten = $this->post('id_konten');
		
		$message = [
			'status'=>1,
			'message'=>'Rate to Normal Success',
		];
		
		$this->Votes_model->RatesNormal($id_user,$id_konten);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function addKontenTraditional_post(){
		$message = [
			'status'=>1,
			'message'=>'Konten Succesfully Added',
		];

		$nama_konten=$this->post('nama_konten');
		$deskripsi_konten=$this->post('deskripsi_konten');
		$gambar_konten=$this->post('gambar_konten');
		$tag_konten=$this->post('tag_konten');
		$lokasi_konten=$this->post('lokasi_konten');
		$id_user=$this->post('id_user');
                $gambarfile=base64_decode($_POST['gambar']);
                $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/traditional_culture_api/uploads/'.$gambar_konten.'', 'w');
	        fwrite($fp, $gambarfile);
           
		$this->Konten_model->AddKontenTraditional($nama_konten, $deskripsi_konten, $gambar_konten, $tag_konten, $lokasi_konten, $id_user);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function kontenAllTraditional_get(){
		$id_user=$this->get('id_user');
		
		$dataKonten = $this->Konten_model->GetAllKontenTraditional($id_user);
		
		if($dataKonten != NULL){
			$message = [
				'dataKonten' => $dataKonten,
				'status' => 1,
				'message' => 'Konten Data Succesfully Retrieved',
			];
		}else{
			$message = [
				'dataKonten' => 'Konten not Found',
				'status' => 0,
				'message' => 'Konten Data Succesfully Retrieved',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}

        public function publicAPIKonten_get(){
                $apikey = $this->get('apikey');
                $filter1= $this->get('filter1');

                $getdata = $this->Konten_model->getPublicKonten($apikey,$filter1);
               
                if($getdata == false){
                     $message = [
				'publicKonten' => 'Data not Retrieved',
				'status' => 404,
				'message' => 'Check your API Key again',
			];
                }else{
                     $message = [
				'publicKonten' => $getdata ,
				'status' => 200,
				'message' => 'Data Succesfully Retrieved',
			];
                }
                
                $this->set_response($message, REST_Controller::HTTP_CREATED);
        } 

        
        public function getHistoryRedeem_get(){
                $id_user = $this->get('id_user');

                $dataHistory = $this->User_model->GetUserRedeemHistory($id_user);

                if($dataHistory == false){
                        $message = [
				'historyData' => 'Data not Retrieved',
				'status' => 0,
				'message' => 'Id User not found or user not redeem reward yet',
			];
                }else{
                        $message = [
				'historyData' => $dataHistory,
				'status' => 1,
				'message' => 'Data Retrieved',
			];
                }
                $this->set_response($message, REST_Controller::HTTP_CREATED);
        }
}
