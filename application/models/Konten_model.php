<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Konten_model extends CI_Model{
	
	function AddKontenTraditional($nama_konten, $deskripsi_konten, $gambar_konten, $tag_konten, $lokasi_konten, $id_user)
	{
		$konten_traditional = array(
			'nama_konten' => $nama_konten,
			'deskripsi_konten' => $deskripsi_konten,
			'gambar_konten' => $gambar_konten,
			'tag_konten' => $tag_konten,
			'lokasi_konten' => $lokasi_konten,
			'id_user' => $id_user,
			'total_rate' => 0,
		);
		
		$this->db->insert('konten_traditional',$konten_traditional);
	}
	
	function GetAllKontenTraditional($id_user)
	{
		$this->db->select();
		$this->db->from('konten_traditional');
		
		$query = $this->db->get();
		
		$result =array();
		
		foreach($query->result() as $row) {
			
			$this->db->select()->where('id_user', $row->id_user);
			$this->db->from('user');
			$userquery = $this->db->get();
			
			$this->db->select('flag_rate')->where('id_user', $id_user)->where('id_konten', $row->id_konten);
			$this->db->from('rate');
			$flagget = $this->db->get();
                        $flaguser = $flagget->row("flag_rate");
                        if($flaguser == null){
                             $flaguser = "-99";
                        }
			
			$result[] = array(
                        'id_konten' => $row->id_konten,
			'id_user' => $row->id_user,
			'nama_konten' => $row->nama_konten,
			'deskripsi_konten' => $row->deskripsi_konten,
			'gambar_konten' => $row->gambar_konten,
			'tag_konten' => $row->tag_konten,
			'lokasi_konten' => $row->lokasi_konten,
			'total_rate' => $row->total_rate,
			'userData'=>$userquery->result(),
			'Flag_rate'=>$flaguser
			);
		}
		
		return $result;
	}

        function getPublicKonten($apikey,$filter1){
                $this->db->select();
		$this->db->from('user_api');
                $this->db->where('API_key like binary',$apikey);
		
		$query = $this->db->get();

                if($query->num_rows()==1){
                        $this->db->set('request_total', 'request_total+1', FALSE);
			$this->db->where('API_key', $apikey);
			$this->db->update('user_api');

                        if($filter1 != NULL){
                        $this->db->select('a.*,b.email');
                        $this->db->from('konten_traditional a');
                        $this->db->join('user b', 'b.id_user= a.id_user', 'full'); 
                        $this->db->where('a.tag_konten like',$filter1);
                        }else{
                        $this->db->select('a.*,b.email');
                        $this->db->from('konten_traditional a');
                        $this->db->join('user b', 'b.id_user= a.id_user', 'full'); 
                        }

                        $querykonten = $this->db->get();

                        $resultkonten = array();
 
                        foreach($querykonten ->result() as $row) {
			     $resultkonten [] = array(
			       'email' => $row->email,
			       'nama_konten' => $row->nama_konten,
			       'deskripsi_konten' => $row->deskripsi_konten,
			       'gambar_konten' => $row->gambar_konten,
			       'tag_konten' => $row->tag_konten,
			       'lokasi_konten' => $row->lokasi_konten,
			       'total_rate' => $row->total_rate,
			       );
		        }

                        return $resultkonten;
                }else{
                    return false;
                }
        }
} 	