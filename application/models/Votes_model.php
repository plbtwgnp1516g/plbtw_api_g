<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Votes_model extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	
	function RatesUp($id_user,$id_konten){
		
		$this->db->select('*');
		$this->db->from('rate');
		$this->db->where('id_user',$id_user);
		$this->db->where('id_konten',$id_konten);
		
		$query = $this->db->get();
		
		$this->db->select('total_rate');
		$this->db->from('konten_traditional');
		$this->db->where('id_konten',$id_konten);
		
		$queryGetTotalRate = $this->db->get();
		$getTotalRate = $queryGetTotalRate->row("total_rate");
		
		$addTotalRate = array(
			'total_rate' => $getTotalRate + 1,
		);
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'id_user' => $id_user,
				'id_konten' => $id_konten,
				'flag_rate' => 2,
			);
			
			$this->db->insert('rate',$data);	
		}
		else
		{
			$data=array(	
				'flag_rate' => 2,
			);
			
			$this->db->update('rate',$data,array('id_user'	=> $id_user,'id_konten' => $id_konten));
		}
		
		$this->db->update('konten_traditional',$addTotalRate,array('id_konten' => $id_konten));	
	}
	
	function RatesDown($id_user,$id_konten){
		
		$this->db->select('*');
		$this->db->from('rate');
		$this->db->where('id_user',$id_user);
		$this->db->where('id_konten',$id_konten);
		
		$query = $this->db->get();
		
		$this->db->select('total_rate');
		$this->db->from('konten_traditional');
		$this->db->where('id_konten',$id_konten);
		
		$queryGetTotalRate = $this->db->get();
		$getTotalRate = $queryGetTotalRate->row("total_rate");
		
		$addTotalRate = array(
			'total_rate' => $getTotalRate -1,
		);
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'id_user' => $id_user,
				'id_konten' => $id_konten,
				'flag_rate' => 1,
			);
			
			$this->db->insert('rate',$data);	
		}
		else
		{
			$data=array(	
				'flag_rate' => 1,
			);
			
			$this->db->update('rate',$data,array('id_user'	=> $id_user,'id_konten' => $id_konten));
		}
		
		$this->db->update('konten_traditional',$addTotalRate,array('id_konten' => $id_konten));	
	}
	
	function RatesNormal($id_user,$id_konten){
		
		$this->db->select('*');
		$this->db->from('rate');
		$this->db->where('id_user',$id_user);
		$this->db->where('id_konten',$id_konten);
		
		$query = $this->db->get();
		
		$this->db->select('flag_rate');
		$this->db->from('rate');
		$this->db->where('id_user',$id_user);
		$this->db->where('id_konten',$id_konten);
		
		$flag_rate_res = $this->db->get();
		$flag_rate_result = $flag_rate_res->row("flag_rate");
		
		if($flag_rate_result == 1){
			$flagcondition = 1;
		}else if($flag_rate_result == 2){
			$flagcondition = -1;
		}
		
		$this->db->select('total_rate');
		$this->db->from('konten_traditional');
		$this->db->where('id_konten',$id_konten);
		
		$queryGetTotalRate = $this->db->get();
		$getTotalRate = $queryGetTotalRate->row("total_rate");
		
		$addTotalRate = array(
			'total_rate' => $getTotalRate + $flagcondition,
		);
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'id_user' => $id_user,
				'id_konten' => $id_konten,
				'flag_rate' => 0,
			);
			
			$this->db->insert('rate',$data);	
		}
		else
		{
			$data=array(	
				'flag_rate' => 0,
			);
			
			$this->db->update('rate',$data,array('id_user'	=> $id_user,'id_konten' => $id_konten));
		}
		
		$this->db->update('konten_traditional',$addTotalRate,array('id_konten' => $id_konten));	
	}
}