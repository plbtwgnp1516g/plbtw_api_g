<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}

	function SignUp($email,$password,$fullname){
                $this->db->select('*')->where('email',$email);
                $this->db->from('user');

                $query=$this->db->get();
                
                if($query->num_rows()==1)
                {
                     return false;
                }else if($query->num_rows()==0){
		      $data = array(
			  	'email' => $email,
				'password' => $password,
				'points_total' => 0,
                                'fullname' => $fullname,
		      );
			
		      $this->db->insert('user',$data);
                      return true;
                }
	}
	
	function Login($email,$password){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		
		$query = $this->db->get();
		
		if($query ->num_rows()==1)
		{	
			return $query->result();	
		}
		else
		{
			return false;
		}
	}
	
	function GetTotalVotes($id_user){
		$this->db->select_sum('total_rate');
		$this->db->from('konten_traditional');
		$this->db->where('id_user',$id_user);
		
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			$totalrate = $query->row("total_rate");
			return $totalrate;
		}
		else{
			return NULL;
		}
	}
	
	function GetTotalPoints($id_user){
		$this->db->select('points_total');
		$this->db->from('user');
		$this->db->where('id_user',$id_user);
		
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			$totalpoints = $query->row("points_total");
			return $totalpoints;
		}
		else{
			return NULL;
		}
	}
	
	function RedeemChallenge($id_user,$challengeno,$addpoint){
		
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id_user',$id_user);
		
		$querypoints = $this->db->get();
		
		if($querypoints->num_rows()>0){
			$points_total = $querypoints->row("points_total");
		
			if($challengeno == 1){
				
				$data=array(
					'challenge_1' => true,
					'points_total' => $points_total + $addpoint,
				);
			}else if($challengeno == 2){
				$data=array(
					'challenge_2' => true,
					'points_total' => $points_total + $addpoint,
				);
			}else if($challengeno == 3){
				$data=array(
					'challenge_3' => true,
					'points_total' => $points_total + $addpoint,
				);
			}else if($challengeno == 4){
				$data=array(
					'challenge_4' => true,
					'points_total' => $points_total + $addpoint,
				);
			}else if($challengeno ==5){
				$data=array(
					'challenge_2' => true,
					'points_total' => $points_total + $addpoint,
				);
			}
			
			$this->db->update('user',$data,array('id_user'	=> $id_user));
			
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where('id_user',$id_user);
			
			$query = $this->db->get();
			
			if($query ->num_rows()==1)
			{	
				return $query->result();	
			}
			else
			{
				return false;
			}
		}else{
			return false;
		}
		
	}
	
	function RedeemRewards($id_user,$minuspoint,$redeemdate,$productname){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id_user',$id_user);
		
		$querypoints = $this->db->get();
		
		if($querypoints->num_rows() > 0){
			$pointstotal = $querypoints->row("points_total");
			
			$pointstotal = $pointstotal - $minuspoint;
			
			if($pointstotal<0){
				return NULL;
			}else{
                                
                                $dataredeem = array(
			  	           'product_name' => $productname,
				           'redeem_date' => $redeemdate,
				           'id_user' => $id_user,
		                        );
			 
		                $this->db->insert('history_redeem',$dataredeem);

				$data=array(
						'points_total' => $pointstotal,
					);
				
				$this->db->update('user',$data,array('id_user'	=> $id_user));	
				
				$this->db->select('*');
				$this->db->from('user');
				$this->db->where('id_user',$id_user);
				
				$query = $this->db->get();

				
				if($query ->num_rows()==1)
				{	
                                        
                                        
					return $query->result();	
				}
				else
				{
					return false;
				}
			}
		}
	}
	
	function GetUserData($id_user)
	{
		$this->db->select('*')->where('id_user', $id_user);
		$this->db->from('user');
		
		$query = $this->db->get();
		return $query->result();
	}

        function GetUserRedeemHistory($id_user){
                $this->db->select('*')->where('id_user', $id_user);
		$this->db->from('history_redeem');
		
		$query = $this->db->get();

                if($query->num_rows()>0){
               		return $query->result();
                }else{
                        return false;
                }
        }
}	