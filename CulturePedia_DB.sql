
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 10, 2016 at 07:53 AM
-- Server version: 10.0.25-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u100948660_plbtw`
--

-- --------------------------------------------------------

--
-- Table structure for table `history_redeem`
--

CREATE TABLE IF NOT EXISTS `history_redeem` (
  `id_history` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `redeem_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_history`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `history_redeem`
--

INSERT INTO `history_redeem` (`id_history`, `product_name`, `redeem_date`, `id_user`) VALUES
(1, 'Steam Wallet IDR 12.000', '07/06/2016', 9),
(2, 'Steam Wallet IDR 12.000', '07/06/2016', 9),
(3, 'Steam Wallet IDR 12.000', '07/06/2016', 9),
(4, 'Steam Wallet IDR 12.000', '07/06/2016', 9),
(6, 'Steam Wallet IDR 12.000', '07/06/2016', 9),
(30, 'Steam Wallet IDR 12000', '10/06/2016', 3),
(29, 'Voucher Diskon Lazada', '10/06/2016', 3),
(28, 'Steam Wallet IDR 12000', '10/06/2016', 3),
(25, 'Steam Wallet IDR 12000', '08/06/2016', 3);

-- --------------------------------------------------------

--
-- Table structure for table `konten_traditional`
--

CREATE TABLE IF NOT EXISTS `konten_traditional` (
  `id_konten` int(11) NOT NULL AUTO_INCREMENT,
  `nama_konten` varchar(255) NOT NULL,
  `deskripsi_konten` longtext NOT NULL,
  `gambar_konten` longtext NOT NULL,
  `tag_konten` varchar(255) NOT NULL,
  `lokasi_konten` longtext NOT NULL,
  `total_rate` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_konten`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `konten_traditional`
--

INSERT INTO `konten_traditional` (`id_konten`, `nama_konten`, `deskripsi_konten`, `gambar_konten`, `tag_konten`, `lokasi_konten`, `total_rate`, `id_user`) VALUES
(1, 'Cik Cik Periuk', 'Lagu ini tidak diketahui siapa penciptanya, namun menurut masyarakat Sambas lagu ini diciptakan oleh orang asli Dayak di Kalimantan Barat.', 'cikcikperiuk.jpg', 'Lagu', 'Kalimantan Barat', 40, 9),
(2, 'Cangget agung', 'Cangget agung merupakan salah satu lagu daerah Lampung. Lagu ini biasanya dinyanyikan ketika ada acara perkawinan dengan memakai adat Lampung dan cakak pepadun (acara begawi untuk mendapatkan gelar adat). Lagu ini menceritakan tentang kekayaan adat budaya Lampung dan ajakan pemuda pemudi Lampung untuk melestarikan budaya Lampung.', 'canggetagung.jpg', 'Lagu', 'Lampung', 400, 3),
(3, 'Ambon Manise', 'Lagu khas daerah maluku', 'ambonmanise.jpg', 'Lagu', 'Maluku', 23, 4),
(33, 'Ampar-Ampar Pisang', 'Pada awalnya dinyanyikan secara iseng saat masyarakat Kalimantan Selatan membuat sebuah kue/makanan yang terbuat dari pisang. Makanan ini bernama rimpi. Cara membuat makanan ini adalah dengan cara pisang di diampar (disusun) kemudian dibiarkan hingga hampir matang mendekati busuk. setelah itu pisang dijemur diampar(disusun) di bawah sinar matahari sampai kira kira pisang mengeras dan mengeluarkan bau manis yang sangat khas.', 'amparpisang.jpg', 'Lagu', 'Kalimatan Selatan', 1, 8),
(29, 'Ayam Den Lapeh', 'Lagu ini dipercayai asalnya merupakan lagu asli orang Minangkabau daripada Padang, Sumatera, Indonesia. Penyanyi asal lagu ini ialah Ally Kasim yang berasal dari Pagar Ruyong, Indonesia.', 'ayamdenlapeh.jpg', 'Lagu', 'Sumatra Barat', 0, 7),
(34, 'Petak Umpet', 'Permainan tradisional Jawa yang dimainkan oleh sekelompok anak-anak yang jumlahnya lebih dari tiga. Dalam permainan ini, salah satu anak akan berperan menjadi kucing. Ia akan mencari teman-temannya yang bersembunyi.', 'petakumpet.jpg', 'Mainan', 'Jawa', 2, 8),
(31, 'Benthik/Gatrik', 'Permainan tradisional Jawa yang dimainkan oleh 2 kelompok anak. Media yang digunakan dalam permainan adalah 2 potong bambu. Satu bambu berfungsi sebagai tongkat dengan panjang 30 cm, sedang satu bambu lainnya berukuran lebih kecil.', 'benthik.jpg', 'Mainan', 'Jawa', -1, 8),
(32, 'Gundu', 'Permainan tradisional Jawa yang hingga kini masih cukup sering dimainkan. Permainan ini mempunyai banyak sekali variasi, namun yang paling sering dimainkan adalah permainan gundu lingkar. ', 'gundu.jpg', 'Mainan', 'Jawa', 0, 7),
(17, 'Egrang', 'Egrang berjalan adalah egrang yang diperlengkapi dengan tangga sebagai tempat berdiri, atau tali pengikat untuk diikatkan ke kaki, untuk tujuan berjalan selama naik di atas. Egrang di Indonesia biasa dimainkan ataupun dilombakan saat peringatan Proklamasi Kemerdekaan Indonesia, 17 Agustus.ketinggian normal.', 'egrang.jpg', 'Mainan', 'Jawa Barat', 1, 8),
(16, 'Gasing', 'Permainan yang cukup sering dimainkan oleh anak-anak Jawa pada zaman dulu. Para pemain dalam permainan ini akan memutarkan gasingnya yang terbuat dari batang kayu.enggunakan seutas tali yang dililitkan. Gasing yang memutar paling lama akan menjadi pemenangnya.', 'gasing.jpg', 'Mainan', 'Kalimantan Barat', 1, 8),
(45, 'tes', 'tes', '20160610_140738_CulturePedia3.jpg', 'Mainan', 'tes', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `id_rate` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_konten` int(11) NOT NULL,
  `flag_rate` int(11) NOT NULL,
  PRIMARY KEY (`id_rate`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id_rate`, `id_user`, `id_konten`, `flag_rate`) VALUES
(4, 3, 1, 2),
(5, 3, 2, 2),
(6, 3, 4, 2),
(11, 8, 7, 0),
(9, 8, 1, 2),
(12, 8, 2, 2),
(13, 8, 8, 2),
(14, 8, 3, 2),
(15, 8, 9, 1),
(16, 8, 10, 1),
(17, 3, 5, 2),
(18, 8, 13, 2),
(19, 8, 31, 1),
(20, 8, 17, 2),
(21, 8, 16, 2),
(22, 8, 33, 2),
(23, 8, 34, 1),
(24, 8, 37, 2),
(25, 81, 16, 0),
(26, 3, 34, 2),
(28, 3, 31, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `points_total` int(11) NOT NULL,
  `challenge_1` tinyint(1) NOT NULL DEFAULT '0',
  `challenge_2` tinyint(1) NOT NULL DEFAULT '0',
  `challenge_3` tinyint(1) NOT NULL DEFAULT '0',
  `challenge_4` tinyint(1) NOT NULL DEFAULT '0',
  `challenge_5` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `fullname`, `password`, `points_total`, `challenge_1`, `challenge_2`, `challenge_3`, `challenge_4`, `challenge_5`) VALUES
(3, 'admin@gmail.com', 'Pier', 'admin', 0, 1, 1, 1, 1, 0),
(4, 'livetodead@gmail.com', 'Budi', 'budi', 0, 0, 0, 0, 0, 0),
(5, 'searchculture@yahoo.com', 'Brandon', 'brandon', 0, 0, 0, 0, 0, 0),
(6, 'yieldrightnow@gmail.com', 'Stark', 'stark', 0, 0, 0, 0, 0, 0),
(7, 'edho@gmail.com', 'Edho Prasetyo', 'test123', 0, 0, 0, 0, 0, 0),
(8, 'galang@yahoo.com', 'Galang Bayu D.', 'galangbede', 0, 0, 0, 0, 0, 0),
(9, 'nico@plbtw.com', 'nico', '1234', 0, 0, 0, 0, 0, 0),
(10, 'pier@gmail.com', 'pier', 'pier123', 0, 0, 0, 0, 0, 0),
(11, 'piere@gmail.com', 'pier', 'pier123', 0, 0, 0, 0, 0, 0),
(12, 'test@gmail.com', 'test', 'test123', 0, 0, 0, 0, 0, 0),
(13, 'admin123@gmail.com', 'test', 'test123', 0, 0, 0, 0, 0, 0),
(14, 'nico@plbtw.cm', 'asd', 'as', 0, 0, 0, 0, 0, 0),
(15, 'absh', 'ajzk', 'ajaja', 0, 0, 0, 0, 0, 0),
(16, 'admina@gmail.com', 'test', 'test123', 0, 0, 0, 0, 0, 0),
(17, 'piersrv@gmail.com', 'pier', 'pier', 0, 0, 0, 0, 0, 0),
(18, 'pier@sss.com', 'pier', 'pier', 0, 0, 0, 0, 0, 0),
(19, 'a@a.com', 'a', 'a', 0, 0, 0, 0, 0, 0),
(20, 'nico@plbtw.co', 'zjzksk', '123', 0, 0, 0, 0, 0, 0),
(86, 'tes', '1', '1', 0, 0, 0, 0, 0, 0),
(85, 'abc', 'abc', 'abc', 0, 0, 0, 0, 0, 0),
(84, 'aa', 'aa', 'aa', 0, 0, 0, 0, 0, 0),
(81, 'a', 'a', 'a', 0, 1, 1, 0, 0, 0),
(83, 'jvjv', 'ibibig', 'aaa', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_api`
--

CREATE TABLE IF NOT EXISTS `user_api` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `API_key` longtext COLLATE utf8_unicode_ci NOT NULL,
  `request_total` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `user_api`
--

INSERT INTO `user_api` (`id_user`, `username`, `fullname`, `password`, `API_key`, `request_total`) VALUES
(18, 'b', 'a a', 'a', 'hOgdSHugFxqTu0d6HH4VW0LSQakwuwMk', 0),
(17, 'a', 'a a', 'a', 'Fl9XROImo4zURbob1MAwjyGb6yN8NwJm', 1),
(16, 'niko', 'niko niko', 'niko', 'OPMIzvXWBl8jlfwwcdcpMXsQr8fFyGvA', 0),
(11, 'Galang', 'Galang Bayu', 'galang', 'yGfUFtUfOfLmuAZk1lIGO8sbbJWGgSw9', 0),
(15, '0', ' ', '0', 'iDbmA2ShkNauCwRXdaLSvIfhMUzAxHGl', 0),
(9, 'edho', 'Edho Prasetyo', 'edho', '5y4XLmC4Ksm1FIBmlAA8JeMhXWlTJxTX', 0),
(19, 'testingweb', 'Test Web', 'test', '1AVcmjzkpdVZ1YSm6ovJDGed890UQQEI', 74),
(20, 'pier', 'Pier Zxc', 'tes', 'FoHB4eBYNwfKkDzB01c1XIvbjFxr4ioo', 0),
(21, 'tes123', 'tes123 tes', 'tes123', 'UCtgHu1gXt8TSedDldYKZbKaGwbPmrzW', 0),
(22, 'kiki', 'Kiki Kaka', 'kiki', 'yD0rE88Xb5PtfhIgQUCJ3i78K4HSqFRA', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
